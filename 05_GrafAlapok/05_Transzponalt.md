# Gráfok

## Transzponált

* Írjunk egy olyan függvényt, aminek bemenete egy éllistával adott irányított gráf, s második paraméterébe, ami szintén egy éllista eljárásszerűen előállítja az eredeti gráf transzponáltját, miközben az eredeti gráfot "leépíti", üres gráffá teszi
* Mi a transzponált?
    * Bár éllistás ábrázolásban utazunk most, de leginkább a mátrixos ábrázolással tudnánk szemléltetni, ott pontosan a mátrixokon értelmezett transzponált fogalomra vezethető vissza
    * Másképpen: ha eddig (u,v) egy él volt, akkor most (v,u) legyen él, avagy fordítsuk meg az éleket!
	* Értelemszerűen irányított gráfra van értelme, az élsúlyozottság mindegy az algoritmus szempontjából
* Ötlet: járjuk be az [1..n] intervallumot (éllisták forrásélei), majd egy belső ciklusban az adott éllistát. Szúrjuk be a node szerinti célcsúcs éllistájához az eredeti gráf éllistájának kiinduló csúcsát, mint értéket (ez pont a külső ciklus i-je)
	* Probléma: Ez nem hatékony, mert ha meg szeretném tartani az éllisták rendezettségét (márpedig igen), akkor a beszúrásnál mindig el kell mennem a céllista végéig a next pointereken keresztül
	* Megoldás1: egy segédtömb létrehozása, ahol a listák végeit tárolom - ez jó, de tudunk jobbat
	* Megoldás2 (erről lesz a struktogram): visszafelé járjuk be az eredeti gráf éllista-tömbjét. Ekkor ha az adott node-ot mindig az adott transzponált éllista elejére szúrjuk be, a későbbi körök mindig hátrébb fogják csúsztatni a korábban beszúrt elemeket (amik ugyebár nagyobb forráscsúcsokhoz tartoznak, tehát pont így lesznek rendezettek a transzponált gráf éllistái)
* Példa:

![Transzponált példa - before and after](05_TranszponaltPelda.png)

```
transpose(adj : E1*[n], adjT : E1*[n])
    adjT := [NULL .. NULL]
    FOR i := n-1 down to 0
        p := adj[i]
        AMÍG p != NULL
            q := p
            p := p->next
            q->next := adjT[q->key]
            adjT[q->key] := q
            q->key := i
        adj[i] := NULL
```

* Minden körben tehát p-vel kiindulunk az éllista első eleméből
    * Amíg ez létezik, kimentjük q-ba, és rögtön tovább is lépünk p-vel
    * Most q-t a kulcsa (célcsúcs) segítségével két lépésben beszúrjuk a megfelelő helyre
    * Most hogy már beszúrtuk a célcsúcs listájába (annak is az elejére), felülírhatjuk a címkéjét a valamikori forráscsúccsal
    * Amikor végigjártuk a listát, magát a listát is ki kell ürítsük, hiszen az az egykori első elemére mutatna még mindig
* A reprezentációt és a lejátszás lépéseit a fenti példára az alábbi ábra szemlélteti
    * Megj.: ez egy régebbi "ábra", egytől n-ig indexelek rajta
    * Megj. 2.: i=2-t nem írtam le külön, hiszen ott nem történik semmi

![Transzponált példa](05_TranszponaltLejatszas.png)
