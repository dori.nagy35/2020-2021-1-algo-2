# Gráfok

## Kifok-befok

* Állapítsuk meg éllistával ábrázolt, súlyozatlan, irányított gráfra a csúcsok kifokait és befokait
    * A függvénynek három paramétere lesz, az első, bemenő paraméter a gráf; a második és harmadik rendre a be- és kifokok. Ezek kimenő paraméternek vannak kezelve, de nem kell referencia jelet írni melléjük, mivel a tömböket eleve referenciaként adjuk át. A két tömb a csúcsokkal van indexelve, az értékek pedig azt mondják meg, mennyi az adott csúcs be- és kifoka

```
degrees(adj : E1*[n], indegrees : N[n], outdegrees : N[n])
    indegrees, outdegrees := [0..0], [0..0]
    FOR i := 0 to n-1
        p := adj[i]
        AMÍG p != NULL
            indegrees[p->key] := indegrees[p->key] + 1
            outdegrees[i] := outdegress[i] + 1
            p := p->next
```

* Az első sor egy szimultán értékadás, ami tehát egyszerre inicializálja mindkét kimenő változót
    * A "[0..0]" jelölje a csupa 0-kat tartalmazó tömböt. A mérete ebből nem derül ki, de mivel tudjuk mekkora a két tömb, nyilván ez is n elemű
    * Persze azt érteni kell, hogy ez a jelölés hiába csak egy sor, valójában egy ciklust helyettesít, ami kifejtve így nézne ki:

```
    FOR i := 0 to n-1
        indegrees[i] := 0
        outdegrees[i] := 0
```

* Ebből következően ennek a sornak Θ(n) a műveletigénye
* Utána még egyszer végigmegyünk a tömbön
    * Erre azért van szükség, azért nem vonható be az inicializálás, mivel ha az i. csúcsnál vagyunk, lehet, hogy egy i+k. még inicializálatlan sornak emelnénk a befokát
* A tömb minden eleméből kiindulva az általa definiált egyszerű láncolt listán is végigmegyünk, és mivel tudjuk, a listaelem kulcsa az általa definiált él célcsúcsa, ezért annak növeljük a befokát; míg az, hogy a tömb hányadik eleme, az a forráscsúcs, ezért annak a kifokát növeljük
* Az, hogy végigmegyünk minden (forrás-)csúcson, és az azokból kimenő éleken, tulajdonképpen azt jelenti, hogy minden egyes élen végigmegyünk. Ott már egy adott élre csak konstans műveletigényű tevékenységeket végzünk, ezért a teljes második ciklus műveletigénye Θ(e)
* Az egész algoritmusé tehát Θ(n+e), amin tovább csak annak ismeretében egyszerűsíthetnénk, ha tudnánk a gráf sűrű-e vagy sem
* Érdekességképpen, ismétlésképpen álljon itt egytől indexelt, szintén n méretű tömbökkel is az algoritmus. Ebben a tárgyban van ilyesmi és így jelöljük:

```
degrees(adj/1: E1*[n], indegrees/1 : N[n], outdegrees/1 : N[n])
    indegrees, outdegrees := [0..0], [0..0]
    FOR i := 1 to n
        p := adj[i]
        AMÍG p != NULL
            indegrees[p->key] := indegrees[p->key] + 1
            outdegrees[i] := outdegress[i] + 1
            p := p->next
```
