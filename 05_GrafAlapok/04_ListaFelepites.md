# Gráfok

## Éllista felépítése absztrakt gráfból

* Legyen adott egy súlyozatlan gráf absztrakt szinten. A feladat legyen az, hogy ebből építsünk fel egy éllistás ábrázolású gráfot
* A feladat megfogalmazása talán magyarázatot kíván: hogy is adhatnék át egy "absztrakt" gráfot, ez olyan, mintha egy "emlőst" adnék át, nem kutyát vagy macskát, pedig hát önmagában ilyen nincs
* Pontosan ez a lényeg. A gráf interfésze, azaz az általa nyújtott "szolgáltatások listája" annyi, hogy vannak címkézett csúcsai és közte húzott élei (esetleg irányok és súlyok). Valami olyan, talán még el sem készített adattípus egy példányát adjuk most át, ami megvalósítja ezt az interfészt, de az algoritmusunknak semmi köze nem lesz ahhoz, hogy ezt hogy teszi. Az is lehet, hogy eleve egy éllistás gráfot adtunk át, aminek persze értelme nem sok, de működik
* Tehát ez egy copy constructor tulajdonképpen
* Az absztrakt gráfot általában g-vel jelöljük, a típusa Graph
    * Ha adott egy g gráf, úgy vesszük, hogy adott vele egy g.V : 2<sup>Vertex</sup> és egy g.E : 2<sup>Edge</sup> változó is (azaz két halmaz, egy a csúcsoknak, egy az éleknek), amik a gráfból lekérhetők

```
toAdj(g : Graph, adj : E1*[n])
    adj := [NULL .. NULL]
    FOR EACH (u, v) ∈ g.E
        q := new E1
        q->key := v
        insert(adj, q, u)
```

* Irányítatlan esetben kiegészülünk még három sorral a szimmetria megtartása miatt:

```
toAdj(g : Graph, adj : E1*[n])
    adj := [NULL .. NULL]
    FOR EACH (u, v) ∈ g.E
        q := new E1
        q->key := v
        insert(adj, q, u)
        q := new E1
        q->key := u
        insert(adj, q, v)
```

* Tehát elsőre egy n méretű, csupa NULL-okat tartalmazó tömbre inicializáljuk az éllistát
* Utána végigmegyünk az összes élen, egy körben úgy fogunk hivatkozni rá, hogy az u-ból v-be menő élről beszélünk
* Mivel az éllistában minden élre egy SL listaelemként (E1*) tekintünk, ezért a konverzió során természetes igény, hogy a heap memórián egy ilyen E1-et létre kell hoznunk
* A q-val jelölt listaelem kulcsa a cél-él, ami itt v, és a forrásél, azaz az u listájába szúrjuk be
* Bár megtanulhattuk, egy new-hoz mindig jár egy delete is, de itt most nem törlünk, hiszen maga a q egy lokális változó, ami a ciklusmag végén magától felszabadul, amire mutat, azt pedig épp, hogy nem akarjuk kitörölni, mert most listát akarunk építeni, pont az a lényeg, hogy az az elem létrejöjjön és ne törlődjön ki. Amire viszont figyelni kell, hogy a ciklusmag újabb körére (azaz amikor létrejön a következő q), a q által mutatott elem valamelyik változónkkal fogva legyen, mert ha egy heapbeli elemre egy pillanatban senki se mutat, az már örökké elveszik számunkra
    * Ezt az insert() működése biztosítja - lásd alább
* Ha a gráf irányítatlan, akkor úgy vesszük, hogy az (u, v) és a (v, u) ekvivalens élek. E-re halmazként nézünk, egy halmazban pedig mint tudjuk, minden elem max. csak egyszer lehet benne. Tehát ha E-ben az (a, b) benne van, akkor biztosak lehetünk benne, hogy csak egyszer van benne, és hogy a (b, a) nincs benne
    * Viszont az éllistás ábrázolásnál definíció szerint elvárt, hogy a visszairányt is behúzzuk, ezért adtuk hozzá azt a 3 sort
* Itt u a célcsúcs és a v listájába szúrunk be
* Ami elsőre problémás lehet, hogy q-t felülírjuk. De mire idáig eljutunk, már úgy vesszük, hogy az insert() már adj egy pointerének értékül adta ugyanazt a memóriacímet, ami q-ban van, tehát q-t biztonsággal "elveszthetjük". Ha q épp mutat valamire, és azt mondom q := new E1, akkor az annyit jelent, hogy mostantól q egy új elemre mutat, nem pedig azt, hogy azt a régi elemet a saját helyén felülírtuk volna

```
insert(adj : E1*[N], q : E1*, u : Vertex)
    HA adj[u] = NULL vagy adj[u]->key > q->key
        q->next := adj[u]
        adj[u] := q
    KÜLÖNBEN
        p := adj[u]
        AMÍG p->next != NULL és p->next->key < q->key
            p := p->next
        q->next := p->next
        p->next := q
```

* A fenti beszúró algoritmus egy az egyben a tavalyi anyagban megtalálható egyszerű láncolt listába (SL) való rendezett beszúrás algoritmusa
    * Az első ágon ha nincs még a listában semmi, vagy ami van, az nagyobb, mint a beszúrandó elem, akkor a q-t a lista elejére szúrjuk
    * A másik ágon pedig feltételezve, hogy az első elem létezik, és nem nagyobb, mint a q kulcsa, merjük "mindig a következőt" nézni, így keressük a beszúrandó helyet, azaz annak megelőzőjét, s végül q-t be is szúrjuk
* Innen pedig egyértelmű, hogy a q felülírható a fő függvényben
* Azért kell rendezett beszúrást alkalmaznunk, mert az éllista elemeiről feltételezzük, hogy rendezettek, az inputról viszont ilyen tudásunk nincs, hiszen absztrakt formában kaptuk a gráfot, az élei egy halmazként foghatók meg, ennek pedig nincs következetes felsorolási sorrendje