# Konzultációs feladatok Gráf alapok/gráfábrázolások témában

## W10 (2020.11.18.) 3. feladata

* Adott egy élsúlyozatlan, irányított g gráf mátrixos ábrázolással
* Számítsuk ki egy másik (szintén paraméterként átadott) mátrixba a g<sup>2</sup>-nek megfelelő gráfot
* Ha g = (V, E) egy gráf, akkor g<sup>2</sup> = (V, E<sup>2</sup>), ahol (u, v) ∈ E<sup>2</sup>, akkor és csak akkor, ha van olyan w ∈ V, hogy (u, w) ∈ E és (w, v) ∈ E
* Azaz egy gráf négyzete az a gráf, amiben azonos csúcsok felett azon csúcsok között vannak direktben élek, ahol az eredeti gráfban kettő hosszú úton lehetett eljutni

### Megoldás

```
square(c : {0,1}[n,n], c2 : {0,1}[n,n])
  FOR i := 0..n-1
    FOR j := 0..n-1
      hasPath := false
      k := 0
      AMÍG !hasPath és k<n
        hasPath := c[i,k] = c[k,j] = 1
        k := k+1
      HA hasPath
        c2[i][j] := 1
      KÜLÖNBEN
        c2[i][j] := 0
```

* Dupla ciklus, hiszen az új, c2 mátrix minden elemének értéket akarunk adni
* Belül egy harmadik ciklus, aminek változójával nézzük a c mátrix k. sorát és oszlopát keresve a köztes csúcsot
* Pesszimista lineáris keresést csináltunk, ha találtunk ilyen k-t, akkor igazzá tehettük a megfelelő cella értékét (azaz behúzhattuk i és j között az élt)

## W11 (2020.11.25.) 2. feladata

* Írjunk hatékony algoritmust, ami a paraméterként megadott, 1-től indexelt mátrixos ábrázolású irányítatlan, élsúlyozott gráfra egy szintén paraméterként átadott megfelelő méretű, 0-tól indexelt tömbbe kiadja rendre a csúcsok fokszámait
* Definíció. A fokszám a csúcsból kimenő élek száma, függetlenül az élsúlyoktól
* Mi a műveletigény n és e függvényében? Miért?

### Megoldás

* Az 1-től indexelés teljesen önkényes alapon történt, nyilván figyelni kell arra, hogy a mátrix sorindexei a tömbhöz eggyel kisebb számokra képződjenek le
* A konzultáción erre jutottunk:

```
degrees(c : R ∪ {∞}[n/1,n/1]; d : N[n])
  FOR i := 1 to n
    degree := 0
    FOR j := 1 to n
      HA c[i,j] != ∞
        degree := degree + 1
    d[i-1] := degree
```

* A mátrixos, élsúlyozott reprezentációban végtelen cellaérték jelzi, ha egy él nincs ott, tehát a "nemvégtelen" értékeket kell összeszámolni
* A mátrix minden során végigmegyünk, azaz minden i-re megnézzük, hogy melyik élekben szerepel az a csúcs "a bal oldalon" - ez persze mindegy, mert az élek irányítottak. Ha találunk egy ilyen élt, az i-nek megfelelő (azaz i-1-es) fokszámot növeljük
* j is az él egy végpontja, de a j fokszámát nem növeljük, hiszen a mátrix szimmetrikussága miatt j is sorra fog kerülni, mint i
* Ha az a felfogásunk, hogy nem lehet hurokél vagy a hurokél csúcsa egyszeresen számítódik, akkor ez az algoritmus jó, ha kétszeresen, akkor minden c[i,i] != ∞ értékre még egyet számolnunk kell
* A degree változó helyett mehetett volna eleve d[i-1]-be a számolás, de akkor is azt 0-ra kellett volna inicializálni
* Egy lehetséges másik megoldás:

```
degrees(c : R ∪ {∞}[n/1,n/1]; d : N[n])
  FOR i := 0 to n-1
    d[i] := 0
  FOR i := 1 to n
    HA c[i,i] != ∞
      d[i-1] := d[i-1] + 1
    FOR j := i+1 to n
      HA c[i,j] != ∞
        d[i-1] := d[i-1] + 1
        d[j-1] := d[j-1] + 1
```

* Itt most egy él feldolgozásakor mindkét végpontjának növeltük a fokszámát
* Mivel irányítatlan, "be van húzva" mindkét irányba az él, tehát a végén az élek számának kétszerese jönne így ki
* Bár nagyságrendileg nem, de összességében azért hatékonyabb az algoritmus, ha ehelyett csak az egyik háromszög-mátrixot járjuk be
* Most a hurokélet se számoljuk kétszer, ha kétszer kéne, akkor a j-s ciklus mehetne i-től, az előtte levő elágazás pedig kidobható lenne
* Mint más példákban (, de ellentétben az itteni másik megoldással), itt is külön ciklusban kell inicializálni az inputot a fals eredmény elkerülése végett
* Műveletigény: Az e az élek száma, tehát ahol nem végtelen az érték. Igaz, ahol végtelen van, nincsenek értékadások, de a ciklus ezeket is bejárja és ellenőrzi, tehát igazából e nem számít a nagyságrendi műveletigény szempontjából (esetleg n<sup>2</sup> + e-nek vehetnénk, ha nagyon alaposak vagyunk). Az első ciklus n-es, a második n<sup>2</sup>-es. Összességében, tehát ez (Θn<sup>2</sup>)
* Ez éles zh feladat volt egykoron, a megoldásokban a fentiek megszegésén túl két gyakori hiba volt:
    * 0-tól indexelés 0..n-ig, nem 0..n-1-ig
    * A szignatúrában a tömb/mátrix elemtípusának kihagyása
