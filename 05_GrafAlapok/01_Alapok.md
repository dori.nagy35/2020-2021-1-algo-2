# Gráfok

* A gráfok dolgok közötti kapcsolatokat írnak le. A "dolgok" a gráf csúcspontjai, a "kapcsolatok" pedig az élek. Minden él két (nem feltétlen különböző) csúcs közötti kapcsolat
* A gráfok nem öncélúak; nyilván valamilyen természetben, társadalomban előforduló jelenség szemléltetésére használhatjuk ezeket, megragadva a fontos, és kifejezetten elhagyva a vizsgálódás szempontjából lényegtelen részleteket
* Azt tudjuk, hogy a gráfok csúcsokból és élekből állnak. Mégis, milyenek lehetnek ezek?
	* Nem számít a csúcs mérete, alakja, elhelyezkedése; az él vastagsága, hossza, színe, stb. Csak és kizárólag az számít, hogy mi mivel van összekötve
	* Ugyanakkor adhatunk a csúcsoknak vagy az éleknek is címkéket aszerint, hogy milyen entitásokat, illetve kapcsolatokat jelölnek. Esetleg (mint ahogy a gyakorlatokon ez sokszor előfordul) a címkehalmazon meglevő rendezés alapján egyfajta sorrendiséget is definiálhatunk így a csúcsok között
	* Speciális esete a címkézésnek az élsúlyozás. Akkor használjuk, ha az adott két csúcs közötti kapcsolatnak van valamiféle "költsége" (pl. ez lehet két város közötti út hossza). Előfordulhat a negatív élköltség is, más kérdés, ennek mikor van igazán értelme
	* Nevezetes gráfalgoritmusok során előfordulhat, hogy a csúcsokhoz plusz attribútumokat (pl. d és π), illetve színezést vezetünk be jelölvén, hogy épp hol tartunk az algoritmusban. Ez is egyfajta csúcscímkézés
	* Nem kötelező élsúlyokat megadni; a súlyozott eset tekinthető általánosabbnak, hiszen az élsúlyozatlan gráfokat simán tudjuk azonos (praktikusan 1-es) élsúlyokkal ábrázolni, mint élsúlyozott gráfok
	* A csúcsok közötti éleknek a súlyozás/címkézés mellett még iránya is lehet. Ez sem kötelező, de az előzőhöz hasonlóan, az irányított eset az általánosabb. Irányítatlan gráfot könnyedén tudunk irányítottként ábrázolni: mindkét irányba behúzzuk az adott éleket
* Gráftípusok és jellemző használati esetek:
	* Irányítatlan és élsúlyozatlan
		* Csúcsok: emberek; élek: ismerősök-e facebookon
	* Irányítatlan és élsúlyozott
		* Csúcsok: városok; élek: utak közöttük (hogy van-e egyáltalán), súlyok: a távolság km-ben
	* Irányított és élsúlyozatlan
		* Csúcsok: dolgozók; élek: ki kinek a főnöke (szervezeti diagram)
	* Irányított és élsúlyozott
		* Csúcsok: házak címei; élek: utak közöttük, súlyok: a távolság (egyirányú utcák esetén előfordulhat, hogy A és B között más a költség, mint B és A között, már ha egyáltalán elérhető)

## Alapfogalmak, ábrázolások

* Absztrakt szinten G = (V,E)
* A G a gráf, a V a csúcsok (vertex, tsz.: vertices), az E az élek (edge) halmaza
* A V elemeit a bejárások és az algoritmusok során néha nagyvonalúan azonosként kezeljük saját címkéjével. Kódban ez persze bonyolultabb, bár a használat helyén látszólag egész egyszerű is lehet - asszociatív tömbök, mapek esete - persze itt fontos, hogy az indexelés hasonlóan hatékony legyen, mint a természetes számokkal működő tömbindexelés esetén
* Az E elemei felfoghatók V feletti pároknak, azaz E része V×V halmaznak (azaz ha a és b két csúcs, akkor az (a,b), de akár az (a,a) is egy lehetséges él)
	* Irányított esetben persze a párok is irányítottak
* Hurokélnek hívjuk az (a,a) alakú éleket
* Párhuzamos élnek hívjuk azt a két élet, ami ugyanazon csúcsok között húzódik &ndash; irányított esetben az (a,b) és a (b,a) élpár nem párhuzamos
* Ha E = V×V, azaz ha minden lehetséges él be van húzva, azt teljes gráfnak hívjuk
* Egy csúcsból kifutó élek száma a csúcs kifoka, a befutó élek száma a befoka
* Irányítatlan esetben ezt együtt fokszámnak nevezzük
* Ha egy csúcsnak nincs egy szomszédja se, azt izolált csúcsnak hívjuk
* Ha a gráfban előfordul olyan részgráf (azaz a V egy részhalmaza a felette értelmezett élekkel), aminek csúcsaiból nem vezet az eredeti gráfban él egy másik hasonló részgráfba, akkor az a gráf két komponense
* Következetesen a V méretére az n és az E méretére az e jelöléseket fogjuk használni
* Bevezetjük a w : E → R ∪ {∞} függvényt, ami minden E-beli élre megadja annak élsúlyát. A végtelen érték azt jelenti, hogy az adott él nem is létezik
    * Tehát maga a valid élsúly nem lehet végtelen
* Ahogy az élek halmazának definíciójából is kiderül, a modellünk nem támogatja a párhuzamos éleket - akkor az már nem V×V részhalmaza lenne, hiszen (a,b) és (a,b) nem különböző elemei ezen halmaznak. A gráfokról azt is fogjuk feltételezni, hogy hurokéleket sem tartalmaznak. Ettől függetlenül, az algoritmusok egy része működik páromzamos éleket illetve hurokéleket tartalmazó gráfokra is, az ilyen esetekben ezt jelezni is fogom