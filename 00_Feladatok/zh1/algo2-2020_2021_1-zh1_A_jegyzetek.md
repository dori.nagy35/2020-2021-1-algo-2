# Jegyzeteim az első zh A csoportjának megírása közben

* Link a megoldás menetéhez [itt](https://youtu.be/fMJvcD3eyz0)
* Megoldások print screenje eme fájl mellett ebben a mappában
* Hibáim okai az adott feladatnál
    * Spoiler: egy dolgot elnéztem, a többinél rossz a feladat
    * Ez éles zh volt, a vizsga is ilyen lesz, hangsúlyozottan mondom, reklamáljatok, ha valami nem stimmel
    * Amúgy a feladatok nagyon ötletesek és szerintem reális nehézségűek, kellően kreatívak, de nem igazán szívatósak, de sajnos az ilyen elgépelés (remélem az volt) mindig benne van a pakliban

# 1. - Tömörítős (hibátlan lett)

```
  Ó7   D8    J11 F12
    \ /        \ /
     15         23    T24   G25
       \       /        \   /
        \     /    M44    49
  A30      38        \    /
     \     /           93
E50    68              /
   \   /              /
     118            0/
        \           /
        1\         /
           211
           
00 10 00 10 1111
M   E  M E
```

* Megj.: lusta voltam átrajzolni: a gyökérnél balra 1, jobbra 0, a többinél fordítva!

# 2. - Szélességi bejárásos (hibátlan lett)

```
BA   BB

   GC
```

# 3. - Szélességi bejárásos (hibák lent)

```
    1  2  3  4  5  6  7  8  9
d   v  v  v  0  v  v  v  v  v
pi  n  n  n  n  n  n  n  n  n

q = 4

1. menet:
    1  2  3  4  5  6  7  8  9
d   v  1  v  0  v  1  v  v  v
pi  n  4  n  n  n  4  n  n  n

q = 2;6

2. menet:
    1  2  3  4  5  6  7  8  9
d   2  1  2  0  2  1  v  v  v
pi  2  4  2  n  2  4  n  n  n

q = 6;1;3;5

3. menet:
    1  2  3  4  5  6  7  8  9
d   2  1  2  0  2  1  v  v  v
pi  2  4  2  n  2  4  n  n  n

q = 1;3;5

4. menet:
    1  2  3  4  5  6  7  8  9
d   2  1  2  0  2  1  3  v  v
pi  2  4  2  n  2  4  1  n  n

q = 3;5;7

5. menet:
    1  2  3  4  5  6  7  8  9
d   2  1  2  0  2  1  3  v  v
pi  2  4  2  n  2  4  1  n  n

q = 5;7

6. menet:
    1  2  3  4  5  6  7  8  9
d   2  1  2  0  2  1  3  3  v
pi  2  4  2  n  2  4  1  5  n

q = 7;8

7. menet:
    1  2  3  4  5  6  7  8  9
d   2  1  2  0  2  1  3  3  4
pi  2  4  2  n  2  4  1  5  7

q = 8;9

8. menet:
    1  2  3  4  5  6  7  8  9
d   2  1  2  0  2  1  3  3  4
pi  2  4  2  n  2  4  1  5  7

q = 9

9. menet:
    1  2  3  4  5  6  7  8  9
d   2  1  2  0  2  1  3  3  4
pi  2  4  2  n  2  4  1  5  7

q = <>

2.

1  2  3  4  5  6  7  8  9  10
3  8  6  3  2  n  6  7  2  8
```

## Hibajegyzék

* "5. menet végén" - helyesen: 5;7;8
    * És valóban, valószínűleg nem vettem észre a (3,8) élt. Javaslom Nektek, hogy írjátok le mindig a meneteknél, hogy melyik csúccsal foglalkoztok, meglepően könnyű belebonyolódni. Ja, és ne siessetek!

* "8-as csúcs d;pi-je" se 3;5, hanem 3;3 értelemszerű okokból
    * Ez az, amikor egy hiba kettőt okoz
    * ... de még így is szerencsém volt, mert az 1.d feladatnál hálisten nem okozott gondot ugyanez a hiba, nem is értem, hogy nem vettem észre - ugye itt az a szerencse, hogy a 8-as mindegy, hogy a 3-asból vagy az 5-ösből jön, ugyanannyi a távolság
* Amúgy szigurú, mert 14 beírható elem volt, abból 12 lett jó, és 12/14 = a kapott pontszámommal, tehát erre a kettőre 0-t adott, pedig majdnem jók voltak :)

# 4. - AVL ("hiba" lent)

```
2, 4, 6, 8, 10

1. kör
2

2. kör

2+
 \
  4

3. kör
2++
 \
  4+
   \
    6

(++,+)

  4=
 / \
2   6

4. kör

  4+
 / \
2   6+
     \
      8

5. kör


  4+
 / \
2   6++
     \
      8+
       \
        10

(++,+)

  4+
 /  \
2    8=
    / \
   6   10

Melyik: 5
Egymás után: 1

  4++
    \
     8=
    / \
   6   10

  8
 / \
4  10
 \
  6
  
( ( (8) 14= (15) ) 30+ ( ( (36) 38= (40) ) 42- ( 50 ) ) )


       30+
      /   \
  14=      42-
 / \      /  \
8  15    38=  50
        / \
       36  40

insert 41

       30+
      /   \
  14=      42--
 / \      /  \
8  15    38+  50
        / \
       36  40+
            \
             41

(--,+)

       30+
      /   \
  14=      40=
 / \      /  \
8  15    38-  42=
        /    /  \
       36  41   50

insert 51

       30++
      /   \
  14=      40+
 / \      /  \
8  15    38-  42+
        /    /  \
       36  41   50+
                 \
                  51

(++,+)

      40=
     /   \
   30=    42+
  /  \    / \
 14= 38-  41 50+
/  \  |        \
8  15 36        51
```

## Hibajegyzék

* Amire gyanakodtam, a 2. szinthez oda kell írni az = jelet: 14=,38-,41=,50+
* És ez egy egész pontot levon
* Ha ilyet láttok vizsgán, reklamáljatok!

# 5. - B+-fa (hiba lent)

```
{ [ ( 4 5 ) 6 ( 7 8 ) ] 10 [ ( 14 18 23 ) 30 ( 30 33 ) ] } 

         10
       /    \
     6        30
   / \       /    \
4/5  7/8 14/18/23  30/33

insert 10

         10
       /    \
     6         18/30
   / \       /   |     \
4/5  7/8  10/14 18/23  30/33


del 7 from ORIGINAL FA

        10/30
   /       |    \
4/5/8  14/18/23  30/33
```

## Hibajegyzék

* No hát itt is egy hiba volt (hogy elvállaltam a félévet...)
* A megadott válaszom az egyik kérdésre (idézőjelbe tettem):

```
"( 6 ) ( 18 30 )"
```

* Amiket helyesnek elfogad:

```
"( 6 ) ( 18 30 ) "
"(6) (18 30) "
"(6) (18 30) "
"( 6 )( 18 30 ) "
```

* Tehát a végére is kell szóköz... paff...
* Fogalmam sincs ki szerezte ezt a feladatot, de ha ilyen hiba van, légyszi ne hagyjátok annyiban, bárkivel hajlandó vagyok egy életre összeveszni, ha ezt nem adja meg
* Amúgy a feladatleírásban úgy volt, hogy "karakterek között" legyen szóköz, nem "utána", egész jó fej módon elfogad egy csomó kombinációt, épp csak a logikust nem. A végén volt az a " ." rész, amit már akkor se értettem, nekem ebből úgy tűnik, hogy ez direkt volt így, nem elírásképp. Érthetetlen
* Amúgy másik részfeladatnál elfogadta hogy nem raktam a végére szóközt. Fuck logic
* És egész biztos, hogy ez volt a hiba, mert találtam egy olyan tanári nézetet a Canvasban, ami kijelzi, hogy pontosan mi volt a hiba

# 6. - Algoírás (nincs értékelve)

```
checkAndFix(A/1 : Edge*[n], B/1:bit[n,n])
  FOR i := 1 to n
    p := A[i]
    prev := NULL
    p != NULL
      HA B[i,p->key] = 0
        t := p
        HA prev = NULL
          A[i] := p->next
        KÜLÖNBEN        
          prev->next := p->next
        p := p->next
        delete t
      KÜLÖNBEN
        prev := p
        p := p->next
```
