# Beadható kódolásos feladatok

* Elvárások:
    * Bármilyen programozási nyelvet, keretrendszert, stb. lehet használni
    * A nyelvben, vagy valamely libraryjéban levő típus- és algoritmusmegvalósításokat természetesen nem szabad felhasználni
    * Dokumentáció csak abban az esetben kell, ha nem triviális a környezet felépítése, a program futtatása, vagy bármilyen a programmal kapcsolatban felmerülő kérdés magyarázatra vár
    * Nem elvárás, de üdvözlendő (és gyorsítja a javítást) a tesztek jelenléte (unit tesztek, tesztfájlok, szimulációs környezet, vagy akár csak egy lista, hogy milyen inputokkal próbáltad végig)
    * A kódolt típusok, algoritmusok minél inkább hasonlítsanak az órán vett változataikhoz
    * Emellett tartsuk be a választott nyelv konvencióit, tehát lehetőleg ne használjunk egy betűs változóneveket, "E2C" jellegű osztályneveket még akkor se, ha papíron ez volt a stílus
    * Természetesen tömböket 0-tól indexelünk
    * Az üres else-ágakat hagyjuk el, ha a nyelv szemantikája erre lehetőséget nyújt
    * A hatékonyabb algoritmus jobb algoritmus
    * A maximális elérhető pont: 5 pont
    * A pontozás szempontjai (sorrendben) a helyesség (nem csinál olyat, amit nem kéne), teljesség (mindent csinál, amit kéne), az órai megközelítéshez való értelemszerű hűség, hatékonyság, általánosság, olvashatóság
    * Ez azt jelenti, hogy nagyobb baj, ha bug van benne, mintha egy-egy feature hiányzik
    * A beadott megoldásokat értékelem, és ha nem max. pontos, leírom a problémáimat, és visszaküldöm javításra. Hibátlanra időben való javítás esetén jár az 5 pont
    * Mindkét témakörből az egyik feladatot lehet kiválasztani, nem kell előre szólni, hogy melyiket
    * Beadási határidő: ameddig jegyet szeretnél kapni -1-2 nap

## Első adag

* 1.
    * Huffman-kódolás
        * Készítsünk programot, amivel (bináris) Huffman-algoritmussal tudunk tömöríteni
        * Az első három kis szorgalmi adhat segítséget a megoldáshoz
        * A bemenet legyen egy tetszőleges string
            * Építsünk belőle hatékony módon frekvenciatáblát
            * Ebből építsük fel a kódfát, amit láncolt módon reprezentáljunk a tavaly tanultaknak megfelelően, de azokat a konkrét alkalmazásra szabva
            * Rendeljük hozzá a betűkhöz a kódjaikat
            * Végül írjuk ki a kódot
        * A programnak legyen még egy használati esete: megadott kódolt stringből és kódfából állítsa elő az eredeti stringet
            * Ezt a függvényt hívhassuk meg úgy is, hogy már a memóriában van egy, az előző kódolás által előállított fa
            * És úgy is, hogy a fát fájlból olvassuk be. A fájlos "szerializált" alak Rád van bízva, de minél kisebb legyen (nyilván mivel a Huffman-fa elég szigorú szabályokkal épül fel, egy csomó infó elhagyható)
                * Ha nem triviális, írj dokumentációt (vagy kommentet) arról, hogyan is épül fel ez a szöveges reprezentáció
* 2.
    * LZW-kódolás
        * Készíts programot, amivel az LZW-kódolást tudjuk kipróbálni
        * Az input egy tetszőleges string, illetve az ábécéje
        * Ebből elő kell tudni állítani az LZW-kódot
            * Az LZW kód kezdődjön egy számmal, ami megmondja, egy kódkaraktert hány biten tudtunk tárolni. Ezután egy szóköz, majd a kódkarakterek jöjjenek bitsorozatokra konvertálva
            * Ez azért fontos, mert 10 fölötti számoknál nem lenne egyértelmű, hogy két egyjegyű, vagy egy kétjegyű kódkarakter van-e ott
        * A programot dekódolásra is használhassuk, ekkor a kód és a bitsorozat-hossz mellett az ábécé is egy paraméter, amiről feltehetjük, hogy ugyanazokat a betűket tartalmazza ugyanabban a sorrendben, mint eredetileg
        * A program tudja még a naiv kódolást és dekódolást, s lehessen úgy is használni, hogy összehasonlítja mind futási időben, mind tömörítési hatékonyságban az LZW ki-be kódolást és a naiv ki-be kódolást
        * Az alternatív (compressed) és az eredetileg tanult stringtábla közül nincs előírva, melyiket használd
* 3.
    * AVL-fa
        * Írd meg az AVL-fa típust az előadás-jegyzetben bevezetett, de gyakorlaton a törlési algoritmus terén kissé módosított verzióban
        * Tudjunk beszúrni, keresni, törölni kulcs alapján, tudjunk maximumot törölni. Tudjunk rendezni vele, tudjuk fájlba írni és fájlból parse-olni. Figyeljünk a műveletigényekre
* 4.
    * Általános fa
        * Készítsünk általános fa interfészt
        * Ezt valósítsuk meg mindkét tanult módon
        * A fa legyen generikus, mindenféle kulcsos adatot tudjon tárolni (a "kulcsos adat" egy újabb interfész lehet)
        * Írjuk meg rá a 3 tanult bejárást, ahol a process() függvény is egy paraméter, próbáljuk ki, illusztráljuk a főprogramban (pl. minden kulcsot megnövel eggyel)
        * Tudjunk parse-olni fájlból és menteni fájlba, mindkét ábrázolásba és mindkét ábrázolásból (keresztbe is - itt nincsenek elvárásaim a műveletigényre)
* 5.
    * B+-fa
        * Készítsünk B+-fa típust
        * 5 pontért a d érték be lehet égetve 4-re, +1 pontért pedig lehet külső paraméter (persze csak valid értékeket elfogadva)
        * Működjön a beszúrás, keresés, törlés kulcs alapján, tudjunk rendezni vele
        * Az előadáson és gyakorlaton tanult módon állítsa helyre magát a fa beszúrás és törlés esetén

## Második adag

* 1.
    * BFS
        * Készítsünk gráf interfészt
        * Valósítsuk meg mátrixos és éllistás ábrázolással is
        * Lehessen fájlba menteni, és fájlból felolvasni is (keresztbe is működjön)
        * A gráf lehessen irányított és irányítatlan is, ezt lehessen a felületen megadni, és annak megfelelően kezeljük (a kódot pedig minél újrafelhasználhatóbban írjuk meg)
        * Élsúlyozás ne legyen
        * Írjuk meg a szélességi bejárást megadható process() függvénnyel, amit illusztráljunk példával a főprogramban
        * A szélességi bejárást indíthassuk olyan módon is el, ami lépésről lépésre, akár GUI-n, akár konzolon logolja az egyes változók értékeit (pi, d, q, színek)
* 2.
    * Dijkstra
        * Készítsünk gráf interfészt
        * Valósítsuk meg mátrixos és éllistás ábrázolással is
        * Lehessen fájlból parse-olni (kiírni nem kell ezúttal)
            * Legyen olyan opció, ha nem mondom meg az ábrázolást, hogy sűrű gráfokat automatikusan mátrixosan, ritkákat pedig éllistásan parse-ol
        * Lehessen irányított és irányítatlan
        * Mindenképp legyen élsúlyozott
        * Írjuk meg rá a Dijkstra-algoritmust, ami jelezzen, ha azt detektálja, nem alkalmazható
        * A minQ megvalósítás lehessen rendezetlen tömbös és kupacos is (a kupacot is mi írjuk meg (aritmetikai reprezentációval), de elég azokat a műveleteket, amiket muszáj)
        * Lehessen futási időt összehasonlítani egyazon gráfra az alábbi esetekre:
            * Éllistás + rendezetlen tömbös
            * Éllistás + kupacos
            * Mátrixos + rendezetlen tömbös
            * Mátrixos + kupacos
* 3.
    * Prim
        * Készítsünk gráf interfészt
        * Valósítsuk meg mátrixos és éllistás ábrázolással is
        * Lehessen fájlból parse-olni (kiírni nem kell ezúttal)
        * Irányítatlan legyen
        * Lehessen élsúlyozott és élsúlyozatlan is
        * Valósítsuk meg a Prim-algoritmust, mindkét esetre, amivel MST-t tudunk adni a gráfnak
            * Az algoritmus észlelje, ha a gráf nem összefüggő és jelezzen hibát
            * minQ-ra elég az egyik féle implementáció
        * Ha a gráf élsúlyozatlan, vezesse magát vissza a szélességi bejárásra, amit szintén írjunk meg
* 4.
    * Kruskal
        * Készítsünk gráf típust éllistás ábrázolással
        * Írjuk meg a Kruskal-algoritmust, úgy, hogy az eredeti gráffal meghívva, azon nem változtatva egy másik gráffal térjen vissza - az MST-vel
        * A gráf legyen élsúlyozott és irányítatlan
        * Az összefüggőséget nem várjuk el, ezt az algoritmusnak kell ellenőriznie, ha nem összefüggő a gráf, térjünk vissza null-lal
        * Az algoritmus során használjuk az unió-holvan adatszerkezetet, ennek megírása is a feladat része
* 5.
    * Topologikus rendezés
        * Készítsünk gráf típust éllistás ábrázolással
        * A gráf élei legyenek irányítottak és súlyozatlanok
        * Írjuk meg rá a mélységi bejárást, ami adjon vissza egy mélységi fát, valamint rendeljen a csúcsokhoz mélységi és befejezési számokat, az élekhez pedig az éltípusokat
        * Ennek segítségével tudjunk topologikus rendezést előállítani a gráfra
        * Természetesen ha a gráf nem DAG, azt jeleznünk kell