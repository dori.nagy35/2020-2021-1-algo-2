# Algoritmusok és adatszerkezetek 2.

## Órák

### W01 - 2020.09.09.

### W02 - 2020.09.16.

### W03 - 2020.09.23.

* Elmaradt

### W04 - 2020.09.30.

### W05 - 2020.10.07.

* Elmaradt

### W06 - 2020.10.14.

### W07 - 2020.10.21.

* Elmaradt

### W08 - 2020.11.04.

* Órai videó linkje: A B+-fás mappában
* A B+-fákról nem csináltam külön videót, hanem ezen az órán néztük végig a B+-fa mappában levő írásos anyagokat

### W09 - 2020.11.11.

* 1.
    * Általános fa teljesen zárójelezett alakjának kiírása, hierarchikus zárójelezéssel
    * Lásd: Általános fás konzultációs anyag
* 2.
    * Határozzuk meg a testvérek közötti legnagyobb kulcsot - saját megoldás
    * Ellenőrzés alatt, TODO
    * Hivatalos megoldás a központi jegyzetben
* 3.
    * Legtöbb gyerek meghatározása általános fában
    * Lásd: Általános fás konzultációs anyag

### W10 - 2020.11.18.

* 1.
    * Rekurzív, felülről lefelé útkiíró algoritmus általános fára
        * Szülő pointerrel
        * Szülő pointer nélkül
* 2.
    * Általános fa magassága
    * Lásd: Általános fás konzultációs anyag
* Kahoot
* 3.
    * g<sup>2</sup> gráf mátrixos ábrázolásra
    * Lásd: Gráfos konzultációs anyag

### W11 - 2020.11.25.

* 1.
    * B+ fa beszúrás-törlés lejátszós feladat
    * Lásd: B+-fa konzultációs anyag
* Kahoot
* 2.
    * Mátrixos gráf, fokszám algoritmus
    * Lásd: Gráfos konzultációs anyag
* Kahoot
* 3.
    * Éllistás gráf, algoritmus, ami eldönti fa-e
    * Lásd: BFS konzultációs anyag

### W12 - 2020.12.02.

* Első zh infók
    * Órai, erre vonatkozó videórészlet linkje [itt](https://youtu.be/A6r4_GZaLCw)
* Első zh konzultáció
    * Az alábbi feladatok + Kahoot videója elérhető [itt](https://youtu.be/9X3dUsB0KKc)
    * 1.
        * Erdős-szám - szélességi bejárás
        * Lásd: BFS konzultációs anyag
    * Kahoot
    * 2.
        * Általános fa speciális esete - indoklós kérdés
        * Lásd: Általános fás konzultációs anyag
    * 3.
        * AVL-fa integritás-ellenőrző algoritmus
        * Lásd: AVL-fás konzultációs anyag
    * 4.
        * B+-fa hatékonysággal kapcsolatos indoklós kérdés
        * Lásd: B+ fás konzultációs anyag
* A konzultáció során az alábbi feladatokra nem volt már idő, de írásos formában feltöltöttem őket:
    * Naiv-Huffman-LZW kódolásokra vett példa
    * LZW dekódolás algoritmusa
    * AVL-fa lejátszás
    * Speciális AVL-fa konstruálós indoklós kérdés
    * Általános fa "átlaga" - algoritmus
    * Általános fa egyedi bejárása - algoritmus
    * B+-fa lejátszás
* Mindegyik a neki megfelelő konzultációs anyagnál van fenn (első kettő: tömörítés; 3-4.: AVL-fa; 5-6.: általános fa, utolsó: B+-fa)

### W13 - 2020.12.09.

* Órai videó [itt](https://youtu.be/6CS7IKJ9i9o)
* Gépelve a második zh/DFS anyag "központinak szánt" alfejezetében
* Tartalma:
    * DFS (mélységi bejárás) algoritmus ismétlése
    * Kiskérdések, algoritmizálás, lejátszás DFS témában
    * Két kahoot kvíz
    * Kiskérdések DFS élosztályzás témában

### W13++ - pótóra 2020.12.11.

* Órai videó [itt](https://youtu.be/jarcFreqh5g)
* Gépelve a második zh/DFS anyag "központinak szánt" alfejezetében
* Tartalma:
    * Követelményekről
    * DFS élosztályzás témában kérdések
    * DFS alkalmazásai témában kérdések
    * Kahoot
