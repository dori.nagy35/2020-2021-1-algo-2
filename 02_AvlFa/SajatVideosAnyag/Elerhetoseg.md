* [Algo2 2020-2021/1. 02/01 - AVL bevezetés](https://youtu.be/FXVKtTPaUqs)
* [Algo2 2020-2021/1. 02/02 - AVL beszúrás szabályai](https://youtu.be/n_gLFONqrZ0)
* Algo2 2020-2021/1. 02/03 - AVL beszúrás példa - TODO
* [Algo2 2020-2021/1. 02/04 - AVL törlés szabályai](https://youtu.be/nBYIECfoZ9M)
* [Algo2 2020-2021/1. 02/05 - AVL törlés példa](https://youtu.be/_fTvhyuvamM)
* [Algo2 2020-2021/1. 02/06 - Ismétlés: törlés keresőfából](https://youtu.be/3Laud_sZQ0Q)
* [Algo2 2020-2021/1. 02/07 - Vizsgapélda](https://youtu.be/FeixhlMHssA)
* [Algo2 2020-2021/1. 02/08 - AVL törlés algoritmusa](https://youtu.be/TNL3x3ErNZE)
* Algo2 2020-2021/1. 02/09 - AVL beszúrás algoritmusa - TODO
