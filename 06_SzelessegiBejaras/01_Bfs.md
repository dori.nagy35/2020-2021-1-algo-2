# Gráfok

## Szélességi bejárás

* Angolul Breadth-first Search (BFS)
* Az alapfeladat: adott egy g gráf és annak egy s csúcsa. Járjuk be az összes s-ből elérhető csúcsot s-ből kiindulva mégpedig olyan sorrendben, hogy s-től egyre távolodjunk: kezdjünk s-től, majd járjuk be s szomszédait, majd annak a szomszédait (azaz az s-ből két lépésből elérhető csúcsokat), s így tovább
* Probléma: mit csináljunk azokkal, akiket már meglátogattunk és most egy másik úton újra sorra kerülnének?
	* Megoldás: semmit, hiszen ha már megvoltak, az rövidebb út volt, mint a mostani, azaz a szélességi bejárás feladatát már elvégeztük erre a csúcsra, és várhatóan annak szomszédai is hamarosan sorra kerülnek
	* Hogy tudom ezt biztosítani? Amikor egy adott csúcsot feldolgozok, berakom egy sorba a szomszédait. Ha olyan csúccsal találkozok, ami benn van már ebben a sorban, akkor biztos lehetek benne, hogy sorra fog kerülni valamikor a "közeljövőben", mégpedig azért, mert valamelyik nemrég befejezett csúcs gyereke volt. Ekkor tehát nem kell újra berakni a sorba, és főleg nem kell újrarendezni a sort. Az is előfordulhat, hogy egy csúcs nincs a sorban, de mégse kell újra feldolgozni. Ez akkor lehet, ha már nincs a sorban, mert már feldolgoztam és kikerült onnan. Ezért mégse elég a sor, mellette színezéssel kell jelölnünk az egyes csúcsok állapotát
		* Legyenek fehérek a még érintetlen csúcsok
		* Szürkék a listában váró csúcsok
		* Feketék a befejezett csúcsok
	* Kezdetben minden csúcs fehér, a sor üres. Aztán berakom a startcsúcsot a sorba, és egyben be is szürkítem. Egy ciklussal addig megyek, míg ki nem ürül a sor. Kiveszem a legrégebb óta bentlevő elemet, befeketítem, majd fogom a fehér gyerekeit, berakom a sorba és beszürkítem őket
* A fenti bejárás melléktermékként még meghatározza minden csúcs az eredetitől vett távolságát (az élszámra legrövidebb út éleinek száma) és egy π attribútum segítségével (ami minden csúcsnak a bejárás szerinti szülőjét adja meg) egy úgynevezett szélességi fát is ami az eredeti gráf részfája és a startból minden csúcsot a lehető legrövidebb úton ér el

```
breadthFirstSearch(g : Graph, s : Vertex)
    FOR EACH v ∈ g.V
        v.c := W
        v.d := ∞
        v.π := NULL
    q := Queue()
    s.c := G
    s.d := 0
    q.add(s)
    AMÍG !q.isEmpty()
        u := q.rem()
        u.c := B
        process(u)
        FOR EACH v ∈ neighbours(u)
            HA v.c = W
                v.c := G
                v.d := u.d + 1
                v.π := u
                q.add(v)
```

* Műveletigény:
    * Az első ciklus értelemszerűen Θ(n)
    * A középső blokk, tekintve, hogy sort létrehozni, és mindenféle műveleteket végezni rajta konstans - konstans
    * Az utolsó ciklusban pedig végigmegyünk az s-ből elérhető csúcsokon, és azon belül minden csúcsra annak kimenő élein. Azaz maximum az összes élen, de amennyiben vannak s-ből elérhetetlen csúcsok, úgy az összes élnél kevesebben. Ezt röviden úgy mondjuk: Ο(e)
* Nem összefüggő esetben a szélességi fa értelemszerűen nem feszítőfa, hanem egy olyan gráf, ami egy fából, plusz izolált csúcsokból áll
    * Átalakíthatjuk úgy az algoritmust, hogy figyelje, minden csúcsot érintettünk-e, és ha a végére marad olyan, amit nem, abból kiindulva újraindítjuk az algoritmust, s így tovább
* A fenti algoritmus egy általános bejáró-felsoroló algoritmus. A queue, illetve a felderített csúcsok színezése ami igazán fontos, a többi adattag már az alkalmazásoktól függően esetleg elhagyható

### Példák

* Egy irányított gráfos példa:

![Szélességi bejárás példa](01_BfsPelda.png)

* És egy irányítatlan gráfos (itt csak egy sorba írom az eredményt, mert minden csúcs értékei garantáltan egyszer frissülnek)

```
1---2   3---4
|   |  /|  /|
|   | / | / |
|   |/  |/  |
5   6---7---8
```

* Legyen a startcsúcs a 2-es

| (d, π) |    1.   |    2.   |    3.   |    4.   |    5.   |    6.   |    7.   |    8.   |
| ------ | ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- |
| INIT   | (∞, ⍉) |  (0, ⍉) |  (∞, ⍉) |  (∞, ⍉) |  (∞, ⍉) |  (∞, ⍉) |  (∞, ⍉) |  (∞, ⍉) | 
| END    | (1, 2)  | (0, ⍉)  |  (2, 6)  | (3, 3)  | (2, 1)  | (1, 2)  | (2, 6)  | (3, 7)  |

* És a szélességi fa:

```
1---2   3---4
|   |  /
|   | /
|   |/
5   6---7---8
```
