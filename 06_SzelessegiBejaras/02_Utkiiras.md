# Gráfok

## Szélességi bejárás

### Út kiírása

* Bemenet: s csúcs, a szélességi bejárás startcsúcsa; u csúcs, az egyik célcsúcs
    * Tegyük fel, hogy s és u egy gráf része, és előzőleg erre a gráfra s startcsúccsal lefuttattunk egy szélességi bejárást, ami sikeresen terminált, a π értékek ennek megfelelőek
* Kimenet: kimeneti folyam, amire írjuk ki az s-ből u-ba vezető csúcsokat ebben a sorrendben. Amennyiben nincs ilyen út, írjuk ki ezt
* Ötlet - ezt most nem írjuk meg: használjunk vermet. Menjünk π szerint u-tól kezdve egészen s-ig, és rakjuk be egy verembe a csúcsokat. A végén ezen a vermen kell végigmenni
* Ötlet2 - ezt igen, lásd alább: rekurzió. A lényeg az, hogy a rekurzív hívás a kiírás előtt van, így végül is sorban fogja kiírni a csúcsokat
	* Probléma: mi van ha félúton derül ki, hogy nincs is út? Ekkor már kiírtuk az út végét, ami elég furán néz ki
		* Megoldás1: ne kiírjuk, hanem konkatenáljuk össze egy stringbe (vagy sorozatba) és a végén csak akkor írjuk ki, ha minden rendben van
		* Megoldás2: ez amúgy se reális gond, hiszen ha u.π nem NULL, akkor a szélességi bejárás során eljutottunk oda. Mégpedig s-ből kiindulva. Tehát (u.π).π se lesz null, és így tovább, visszafelé is meglesz az út
		    * Egy esetben azért mégis lehet u.π = NULL úgy, hogy van út..., amikor vagy eleve, vagy a rekurzív hívással u egyenlő lesz s-sel..., na de akkor el se jutottunk ide, mert a külső if utunkat állta

```
printPath(s : Vertex, u : Vertex) : OutStream
    out := <>
    printPath(s, u, out)
    return out
```

```
printPath(s : Vertex, u : Vertex, out : OutStream)
    HA s = u
        out.print(s)
    KÜLÖNBEN
        HA u.π = NULL
            out.print("nincs út")
        KÜLÖNBEN
            printPath(s, u.π, out)
            out.print(u)
```