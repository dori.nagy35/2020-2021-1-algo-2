# Konzultációs feladatok B+-fa témában

## W11 (2020.11.25.) 1. feladata

* Végezzük el az alábbi műveleteket a megadott kiinduló B+-fán (i - insert, d - delete)
    * <i-32, i-7, i-28, i-26, d-29, d-26, d-6, d-8, d-7>
    * Kiindulás:

```
        8/23/29
       /  / |  \
      /  /  |   \
     /  /   |    \
2/3/6 8/10 23/25 29/35/45
```

### Megoldás

* i-32
    * 29 < 32, ezért a jobb szélsőbe szúrjuk be, ami viszont így megtelt, ketté kell választani
    * Lesz egy 29/32 és egy 35/45-ös csoport, ami miatt a szülőbe újonnan a 35 másolódik fel
    * A szülő is megtelt: 8/23/29/35
    * A szülő már nem levél, ezért a ketté választásánál lesz egy 8/23-as, és egy olyan, ahol a kisebbik kulcs nem másolódik, hanem felmegy az új gyökérbe, azaz egy 35-ös, és egy 29-es gyökér:

```
               29
              /   \
             /     \
         8/23       35
        / | \        | \
       /  |  \       |  \
      /   |   \      |   \
     /    |    \     |    \
2/3/6  8/10  23/25  29/32  35/45
```

* i-7
    * A legbaloldalibba kerül
    * Ami megint szétválik: 2/3 és 6/7-re
    * Az új hash-kulcs a szülőbe a 6-os
    * A szülőben ez el is fér, ezért itt megáll az algoritmus

```
              29
             /   \
            /     \
           /       \
       6/8/23       35
      / / | \        | \
     / /  |  \       |  \
    / /   |   \      |   \
   / /    |    \     |    \
2/3 6/7  8/10  23/25  29/32  35/45
```

* i-28
    * 28 < 29, ezért balra megyünk; 28 > 23, ezért jobbra
    * 23/25 mögé be is fér, kész vagyunk, nem frissül szülőben semmi

```
              29
             /   \
            /     \
           /       \
       6/8/23       35
      / / | \        \ \
     / /  |  \        \  \
    / /   |   \        \   \
   / /    |    \        \    \
2/3 6/7  8/10 23/25/28  29/32  35/45
```

* i-26
    * 26 < 29, ezért balra, 26 > 23, ezért jobbra megyünk
    * Betelt, kettéválunk: 23/25 és 26/28-ra
    * A szülőbe a 26 megy fel, tehát az is ketté válik: 6/8-ra és 23/26-ra válna, de ebből a kisebbik kulcs nem átmásolódik, hanem áthelyeződik a szülőjébe, tehát 26 marad csak itt, a szülőhöz kerül a 23, aminek van is ott helye

```
             23/29
            /  |   \
           /   |     \
          /    |       \
       6/8     26       35
     / | \     |  \      \ \
    /  |  \    |   \      \   \
   /   |   \   |    \      \    \
2/3 6/7  8/10 23/25 26/28  29/32  35/45
```

* d-29
    * 29 >= 29, ezért jobbra, majd 29 < 35, ezért balra megyünk
    * Kitöröljük a 29-et, 32 egyedül marad, és a szomszédjától (35/45) se tud lopni, egyesül hát a két levél
    * A szülőből törlődik a 35, mint hash-kulcs, tehát 0 kulcsa, 1 pointere van, ami nem kóser, átvenni nem tud, ezért egyesül a szomszéddal
    * A szomszéd kulcsa a 26, ez teljesen jó is, elválasztja az újdonsült 3 pointerű csúcs két bal oldali elemét. A jobb oldalit pedig a szülőből leeső 29-es fogja elválasztani

```
               23
              /  \
            /      \
          /          \
      6/8             26/29
     / | \           /  |  \
    /  |  \         /   |   \
   /   |   \       /    |    \
2/3 6/7  8/10   23/25  26/28  32/35/45
```

* d-26
    * 26 >= 23, ezért jobbra megyünk, majd mivel 26 >= 26 és 26 < 29, ezért a középső levelet választjuk
    * Kitöröljük a 26-ot, a 28 egyedül marad, de a jobb oldali szomszédjától át tudja venni a legkisebb elemet
    * Felfelé frissíteni kell a hash-kulcsot

```
               23
              /  \
            /      \
          /          \
      6/8             26/35
     / | \           /  |  \
    /  |  \         /   |   \
   /   |   \       /    |    \
2/3 6/7  8/10   23/25  28/32  35/45
```

* d-6
    * 6 < 23, ezért balra megyünk, 6 >= 6, de kisebb, mint 8, ezért utána középre
    * Töröljük a 6-ot, 7 egyedül marad, nem tudunk lopni, ezért egyesül a szomszédjával, megalkotva a 2/3/7-es levelet
    * Felfele ez abban nyilvánul meg, hogy a 6-os kulcs törlődik

```
           23
          /  \
         /     \
        /        \
       8          26/35
      / \        /   |  \
     /   \      /    |   \
    /     \    /     |    \
2/3/7  8/10   23/25 28/32  35/45
```

* d-8
    * 8 < 23, ezért balra, 8 >= 8, ezért jobbra megyünk, s töröljük a 8-at
    * 10 egyedül marad, átveszi a 7-est a szomszédtól
    * Felfelé frissül a határoló kulcs

```
           23
         /    \
        /      \
       /        \
      7         26/35
     /\        /   | \
    /  \      /    |  \
   /    \    /     |   \
2/3  7/10  23/25 28/32  35/45
```

* d-7
    * 7 < 23, ezért balra megyünk, saját magánál nem kisebb, ezért utána jobbra
    * Töröljük a 7-est, nem tud szomszédtól átvenni, ezért a 2/3/10 egyesül
    * Felfelé a 7-es köztes kulcs kiesik, mivel testvértől tud átvenni, ezt megteszi
    * Ilyenkor a hasító kulcsok úgy mozognak, hogy a szülő 23-asa megy a 7 helyére, a 26 pedig a szülőbe, ami helyébe nem kerül semmi, hiszen az ex-7-es megkapta a 23/25-öt, tehát itt csak a 26/35-ös csúcsnak csökken a gyerekszáma, s ezáltal a kulcsszáma

```
           26
         /    \
        /      \
       /        \
      23         35
     /\          /  \
    /  \        /    \
   /    \      /      \
2/3/10  23/25 28/32  35/45
```

## W12 (2020.12.02.) 4. feladata

* Egy d fokú, h magas, m csúcsú, n adatot tartalmazó, a gyökerével adott, helyes B+-fára nagyságrendileg mi a műveletigénye az n db tárolt adat kulcsának növekvő kiírásának?
* Részletesen indokold, anélkül 0 pont

### Megoldás

* Előbb értsük meg, mi mit jelent:
    * d fokú: max ennyi gyereke lehet egy belső csúcsnak, illetve ennyi-1 kulcs egy levélben
    * h magas: ha keresek egy konkrét kulcsot, ennyi lépésben találom meg a levélblokkját (mert összesen h+1 db szint van), ahol további d-2 lépést kell megtennem legrosszabb esetben a konkrét slothoz (mert eleve az elsőnél vagyok, és összesen d-1 lehet, a legrosszabb eset pedig a legutolsó)
    * m csúcsú: összesen ennyi node van, igazából esetünkben ez irreleváns, mert a levélszint fog csak számítani. Igaz, ebből, az n-ből és a d-ből lehetne erre becslést adni, de nem lesz rá szükség
    * n adatot tartalmazó: ennyi értékes adat van a levélszinten, ez nyilván ennél a fánál nem egyezik meg az összes csúcs számával, sőt a levélszint csúcsszámával sem, hiszen egy csúcsban max. d-1 adat lehet
* Amit még tudnunk kell:
    * A levelek a levélszinten láncolt listaként viselkednek, sőt, a levél node-ok egymással is össze vannak kötve - még azok is, akik nem testvérek, hanem (sokad) unokatestvérek is!
    * Ráadásul ezek szigorúan monoton növekvő sorban vannak
    * Tehát a szomszédosok vannak összekötve
    * És minden levél kulcsai szigorúan monoton növekvők és a levelek is egymáshoz képest
* Innen nem túl bonyolult: keressük meg az elsőt, majd menjünk végig
    * Az első az Θ(h) - azért Θ, mert minden út azonos magas
        * Ez valamilyen logos viszonyban lesz m-mel, de egy bizonyos elemszám felett biztosan n-nél is nagyságrendileg kisebb lesz
    * Ha megvan az első, akkor gyakorlatilag egy n hosszú láncolt listán kell végigmenni - ha egy node-on belül nem láncolt listaként szerepelnek a kulcsok, akkor is végig kell rajtuk mennünk, tehát ez Θ(n)
* Mivel n nagyobb, mint h, ez az egész Θ(n)
* Amúgy, ha nem lennének láncolt listába kötve a levelek, akkor az összes lehetséges levélig le kéne menni (tehát gyakorlatilag preorder be kell járni a fát), és ott azokon végigmenni. A végigmenés költsége egy levélre kb. d, de pont annyi levél van, hogy ez összesen n legyen. A levelek megkeresésének költsége annyi, hogy minden lehetséges úton menjünk le egészen a levélszintig: ez a gyökérnél kb. d, a gyökér gyerekeinél is, tehát már d*d, és így tovább, összesen h-szor. Tehát ennek a műveletigénye így Θ(d^h) lenne

## W12 (2020.12.02.)-be be nem férő feladat

* Az alábbi 4-edfokú B+-fából kiindulva végezzük el sorban az alábbi lépéseket (i–beszúrás, d–törlés) 
* Minden lépés után rajzoljuk újra a fát
* <i–42,i–41,i–33,i–36,d–42,d–38,d–15>

```
            35/85
           /  | \
          /   |  \
         /    |   \
        /     |    \
10/15/25  35/38/45  85/87/95
```
* Ez amúgy ebben a sorrendben állt elő: 10, 25, 35, 95, 85, 45, 15, 38, 87

### Megoldás

* i-42

```
            35/42/85
           / | \    \
          /  |  \    \
         /   |   \    \
        /    |    \    \
10/15/25  35/38  42/45  85/87/95
```

* i-41

```
            35/42/85
           / |   \  \
          /  |    \   \
         /   |     \    \
        /    |      \     \
10/15/25  35/38/41  42/45  85/87/95
```

* i-33

```
                 42
                /  \
               /    \
              /      \
         25/35        85
        /|  \          |\
       / |   \         |  \
      /  |    \        |    \
     /   |     \       |      \
10/15 25/33 35/38/41   42/45  85/87/95
```

* i-36

```
                     42
                    /  \
                   /    \
                  /      \
                 /        \
         25/35/38         85
        /| \   \          |\
       / |  \   \         |  \
      /  |   \   \        |    \
     /   |    \   \       |      \
10/15 25/33 35/36 38/41   42/45  85/87/95
```

* d-42

```
                     42
                    /  \
                   /    \
                  /      \
                 /        \
         25/35/38         87
        /| \   \          |\
       / |  \   \         |  \
      /  |   \   \        |    \
     /   |    \   \       |      \
10/15 25/33 35/36 38/41   45/85  87/95
```

* d-38

```
                  42
                 /  \
                /    \
               /      \
              /        \
         25/35          87
        /| \            |\
       / |  \           |  \
      /  |   \          |    \
     /   |    \         |      \
10/15 25/33 35/36/41   45/85  87/95
```

* d-15

```
               42
              /  \
             /    \
            /      \
           /        \
         35          87
        / \          |\
       /   \         |  \
      /     \        |    \
     /       \       |      \
10/25/33 35/36/41  45/85  87/95
```
