# Pontozási rendszer

## Nagyobb dolgok

### Zárthelyi

* Két alkalom
    * Sőt, technikailag több "első" alkalmat is kiírok, lásd Teamsen
* Van pótalkalom mindkettőhöz, rontani nem lehet
* Ha így se elég, lesz egy utolsó, azaz kb. "gyakuv", ami felülírja a két zh pont összegét, a többi lehetséges pontszámra nem hat, rontani nem lehet
* Első zh témakörei: szélességi bejárásig bezárólag minden
* Második zh témakörei: mélységi bejárástól kezdve minden
* Formátum:
    * Két feladat
    * Egy kódírós, egy lejátszós
    * Mindkettő más témában
    * Mindkettőhöz egy vagy több minikérdés
    * Szövegesen kell beadni, gépelve canvason vagy képben Teamsen
* Egy zh 40 pontos
* Nincs minimumpont elvárás

### Szóbeli

* Két alkalom egyedileg megbeszélt időpontban
* Első + második zh témája
* Kb. 10 perc / alkalom, de ahogy alakul
* 3 véletlenszámot generálok, egyik se lehet azonos
    * Mindegyik egy-egy témát jelent
    * Ebből az egyiket kiválaszthatod, a kiválasztottból nem kérdezek
    * A téma egy viszonylag tág dolog, ebből fogok egy (vagy ha úgy alakul) több konkrét kérdést feltenni, amire elvárás, hogy közel azonnal válaszolj
* Mindegyik témára 10 pont jár maximum
* Azaz egy szóbelire 10*2 = 20p
* A két szóbelire összesen 40p
* Van minimumpont elvárás: minden témára 4 pont
* Mindkét szóbeli egyszer javítható

### Megajánlott vizsga ötös

* Megajánlott vizsga 5-öst ér, ha:
    * Ha eléred a minimumpont elvárást szóbeliken
    * És a szóbelikből és írásbelikből összesen elértél összesen 96 pontot (80%-ot)

### Vizsga feltétele

* Legalább 2-es gyakorlati jegynyi pont összegyűjtve
* Vagy még 2020-as vizsgák esetén:
    * Abszolváld az első zh-t és az első szóbelit együttesen legalább 90%-osra
    * Azaz érj el 54 pontot ezekből

## Bónuszpontok

### Első minta zh

* 60 pontos zh
* Legalább 30 ponttól 1 bónuszpont
* Legalább 40 ponttól 2 bónuszpont
* Legalább 50 ponttól 3 bónuszpont

### Canvasos kvízek

* 10 ponttól: 2p
* 7 ponttól: 1p
* Ha több, mint 10 darab ilyen volt, a top10 számít

### Kahootos kvízek

* 10 darab
* Részvételért: 1p
* Top5-beli helyezésért: +1p

### Kis szorgalmik Canvasban

* 24 darab, 22 db 1 pontos, 2 db 2 pontos
* Összesen elérhető: 26p

### 2 kódolós szorgalmi Canvasban

* Első és második zh témájából 1-1
* 5 féléből lehet választani mindkét témakörből, egy írható meg
* Javítható
* 2×5p

### Órai munka

* Eseti jellegel 1-1p

### Hibabejelentés

* Gitlab, canvas, bárhol
* Kis hibák: minden 5. után 1p
* Nagy hibák: 1p
* Ha gitlab -> MR

### Sakk

* Aki nyert kahootot, megpróbálhatja
* Ha megver: 1p

# Pontszámítás alapja

* Ketteshez "elvárt":
    * Összes kahooton részt venni (10p)
    * Összes kvízt megcsinálni 7 pontosra (10p)
    * Két 50%-os zh (40p)
    * Szóbelin a minimum (16p)
    * Összesen: 76p
* Maximum reálisan elérhető:
    * Összes kahooton részt venni, kb 3-on benne lenni a top 10-ben (13p)
    * Összes kvízt megcsinálni 7 pontosra, a felét hibátlanra (15p)
    * Két 80%-os zh (64p)
    * Fele kisszorgalmi (13p)
    * 2 kódolós szorgalmi (10p)
    * Hibátlan szóbeli (40p)
    * Plusz még lehetne számolni az órai munkát, sakkot, minta zh-kat, stb., tehát ez se igazi maximum, de ezzel számolok
    * Összesen: 155p

# Ponthatárok

* Elégséges (2): 76 - 89
* Közepes (3): 90 - 109
* Jó (4): 110 - 129
* Jeles (5): 130 - 155 and beyond

# Szóbeli tételsor

* TBA