# Gráfok

## Mélységi bejárás példa - Élek osztályozása

### Példa

* Tekintsük az alábbi gráfot, járjuk be mélységien

![Példa feladat DFS-re](02_DfsPeldaFa.png)

* Kezdjük pl. az A csúccsal (mivel csak az van megadva, hogy for each ciklusal végigmegyünk, elvileg nincs rögzített sorrend, de minden ismert reprezentáció ezzel kezdene :) )
* Szürke lett, majd megkapta az 1-es mélységi számot. A befejezési szám még 0
* Vesszük sorra a szomszédait: B, D, G. B-vel kezdünk, ő is szürke lesz, a mélységi száma pedig 2. Most ennek a gyerekeivel megyünk tovább, stb.
* Amikor F-nél tartunk, annak A lenne a gyereke, de mivel a színe nem fehér, nem látogatjuk újra. Mivel F-nek nincs több gyereke, megkapja a soron következő befejezési számot (6) és visszalépünk a rekurzió szülő szintjére
* Azt tapasztaljuk, hogy egészen elfogytak az érintetlen szomszédok, visszatértünk A-hoz. Innen van még egy út, D-be, ezt még megejtjük, de onnan is már csak fekete csúcsokba juthatnánk
* Megint visszatérünk A-ba, akinek már nincs több érintetlen szomszédja
* Ezt az állapotot látjuk a következő ábrán:

![Példa részmegoldás DFS-re](02_DfsPeldaReszmeo.png)

* Itt returnolt egészen a visit() függvény, tehát most vesszük a főprogramban a következő csúcsot. Ez a B, de ez nem fehér, megyünk tehát tovább. C-re belépünk újra a rekurzióba
* Ez látogatja H-t, majd visszatér
* Még a főprogram végigiterál az összes maradék (fekete) csúcson, és kész is vagyunk:

![Példa megoldás DFS-re](02_DfsPeldaMeo.png)

* A megoldás a π tömbbel teljes
* Elég csak két sort írnunk, akár a szélességi bejárásnál. Az első az inicializálás, a második a végeredmény - minden csúcsnak csak egyszer adunk szülőt (épp ahogy mélységi és befejezési számot is)

| π    | A    | B    | C    | D    | E    | F    | G    | H    |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| INIT | NULL | NULL | NULL | NULL | NULL | NULL | NULL | NULL |
| DONE | NULL | A    | NULL | A    | B    | G    | E    | C    |

### Osztályozás és DAG-tulajdonság

* Egy gráf DAG (directed acyclic graph), ha irányított és körmentes
    * Azaz nincs benne irányított kör
* Ennek eldöntése a mélységi bejárás egy alkalmazási esete
* DAG-okkal lehet leírni pl. folyamatokat, ahol a csomópontok az egyes elvégzendő munkák, az élek pedig azt jelzik, melyik rész melyik más rész előfeltétele. Itt, ha irányított kör van, akkor nyilván lehetetlen a folyamatot elvégezni
* A DAG-tulajdonság elöntéséhez a mélységi bejárás során érintett éleket fogjuk osztályozni

#### Faélek - F

* Faélnek nevezzük azokat, amelyek a mélységi bejárás során elkészülő mélységi fa (vagyis erdő) élei
* Faél tehát az az él, ami egy olyan csúcsba vezet, amit az algoritmus most fedez fel először, ami mentén tovább hívja a visit() függvényt
* Ha épp (u,v) élt vizsgáljuk, ez faél, ha a feldolgozás pillanatában v fehér, avagy v.d = 0

#### Előreélek - E

* Előreél az, amikor egy már felfedezett csúcsba találunk egy "rövidített utat"
* Tehát amikor a mélységi fa egy eleméből, annak egy leszármazottjába - amit egy másik, potenciálisan hosszabb - úton már megtaláltunk, vezet egy direkt él
* Formálisan így tudjuk eldönteni az éppen vizsgált (u,v) élről, hogy ilyen-e:
    * v.d > 0 és v.f > 0 (azaz v fekete) és u.d < v.d
    * Megj.: v.d > 0 feltételt nem is kell kiemelni, mert egyrészt, ha v.f > 0, akkor ebből következik (hiszen d akkor kap értéket, amikor bele megyünk, f pedig amikor elhagyjuk - ez utóbbi van később); másrészt ha teljesül, hogy u.d < v.d, akkor ebből is következik ez, hiszen ilyenkor u szürke, azaz a d-je már nem 0 (de az f-je igen), és ha egy 0-nál nagyobb számnál nagyobb v.d, akkor biztosan nagyobb 0-nál

#### Visszaélek - V

* Visszaéllel akkor találkozunk, ha a feldolgozás alatt álló csúcs egyik szomszédaképp egy olyan csúcsot találtunk, amiből származik az aktuális csúcs
* Konkrétan, ha (u,v) élt vizsgáljuk és v.d > 0, de v.f = 0, azaz v szürke

#### Keresztélek - K

* Keresztélről akkor beszélünk, ha már felfedezett két mélységi (rész-)fa közötti élt találtunk
* Azaz, ha (u,v)-t vizsgálva v már fekete (már végeztünk vele), azaz v.d > 0 és v.f > 0 és még az is igaz, hogy nem u-ből jőve jutottunk valahogy v-be, azaz u.d > v.d
    * Megj.: itt már fontos a v.d ellenőrzése, mivel az u.d > v.d triviálisan igaz, ha v fehér
* Megj.: ezek alapján látjuk, hogy az előreél "definíciójaként" leírt mondat kissé pongyola, hiszen ha a két résztvevő él különböző részfában van, akkor a "rövidített út" igazából egy keresztélt tartalmaz

#### Konklúzió

* Egyfajta összefoglalásképp a színek és számok viszonyáról:
    * Ha u csúcs fehér, akkor u.d = 0 és ebből következően u.f = 0
    * Ha u csúcs szürke, akkor u.d > 0, de még u.f = 0
    * Ha u csúcs fekete, akkor u.f > 0 és ebből következően u.d > 0
    * Más kombináció nem lehetséges
* Amikor (u,v) élt vizsgáljuk, u biztosan szürke, azaz u.d > 0 és u.f = 0
    * v pedig lehet fehér - faél
    * lehet szürke - visszaél
    * lehet fekete - előreél, ha később találtuk meg u-nál, azaz mivel u még nincs befejezve, ezért belőle származik; és keresztél ha korábban találtuk meg u-nál, ekkor nincs köztük leszármazási viszony
* Mint említettem, az élek osztályozást arra tudjuk használni, hogy eldöntsük, a gráfban nincs-e irányított kör
    * Ha faélt találunk, az nem lehet kör "tünete", hiszen a fában nincs kör
    * Ha előrelt, az irányítás nélkül már kör lenne, hiszen egy út "levágását" jelenti, de ez a DAG-ok alkalmazása szerint (process flow) nem gond, sőt
    * A keresztél két eddig függetlennek tűnő rész közötti kapcsolat, de mivel eddig függetlennek gondoltuk, kört biztos nem jelent
    * A visszaél viszont azt jelenti, hogy egy olyan kapcsolatot fedeztünk fel, ami (akár áttételesen is) eddig fordítva volt jelen. Eddig el tudtunk jutni v-ből u-ba, de (u,v) éllel fordítva is
* Tehát egy gráf akkor és csak akkor DAG, ha a mélységi bejárás során nem detektálunk benne visszaélt
    * Megj.: a hurokél is visszaél, és tökéletesen működik rá a fenti elgondolás és annak algoritmikus megvalósítása is

### DAG-tulajdonságot eldöntő algoritmus

* A mélységi bejárás algoritmusát kell módosítani-bővíteni az alábbi módon
* Amikor a visit() függvényben a v.c = W elágazás igaz ágán vagyunk és meghívjuk újra a visit()-et rekurzívan, akkor feljegyezhetjük, hogy ez az (u,v) faél
* Ennek az else ága alapból egy SKIP, cseréljük ki ezt erre:

```
HA v.f = 0
  (u,v) visszaél
KÜLÖNBEN
  HA u.d < v.d
    (u,v) előreél
  KÜLÖNBEN
    (u,v) keresztél
```
* Megj.: az elágazás - v.f = 0 - ha igaz, akkor v szürke, ha hamis, akkor fekete, mert a fehér ágat már a korábbi elágazással kizártuk
* Megj.: ha persze az algoritmus "csak" DAG-tulajdonságot akar eldönteni, elég csak a visszaéleket figyelni
* Megj.: ekkor voltaképpen a d és f számok nem is kellenek, feltéve ha a színek megvannak. Ez fordítva is igaz. A keresztél-előreél dilemma eldöntéséhez nem elegek a színek, oda kellenek a számok
* Megj.: irányítatlan esetben ha visszaélt találunk, lehet, hogy ez már egy faél újabb feldolgozása. Ezt kezelni kell

### A bemutatott példa élei

![Példa megoldás DFS-re élosztályozással](02_DfsPeldaOsztalyozas.png)