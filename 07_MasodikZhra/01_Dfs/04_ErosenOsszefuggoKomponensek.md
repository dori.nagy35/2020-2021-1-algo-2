# Gráfok

## Erősen összefüggő komponensek

* Egy irányított gráf (vagy annak egy komponense) erősen összefüggő, ha minden u, v csúcsára igaz, hogy van irányított út u-ból v-be és v-ből u-ba is
    * Azaz egy olyan irányított gráf, vagy annak részgráfja, ahol minden csúcs minden másik csúcsból elérhető, noha nem feltétlen teljes gráfról beszélünk
* Eljárás:
    * 1. Futtassuk le a mélységi bejárást tetszőleges sorrendben. π tömböt nem kell meghatározni, de tegyük verembe a csúcsokat befejezéskor
    * 2. Készítsünk egy olyan gráfot, ami az eredeti transzponáltja (azaz ugyanazon csúcsokkal, de fordított élekkel)
    * 3. Futtassuk le újra a mélységi bejárást, de most a transzponált gráfon. A külső ciklusban a csúcsokat kifejezetten a verembeli sorrend alapján válasszuk meg. Azaz kezdjük a veremtető csúcsával, és amikor a visit() függvény teljesen visszatér, folytassuk a verem legfelső még fehér csúcsával s.í.t.
    * 4. A második bejárás szerint egy mélységi fába tartozó csúcsok definiálnak (az eredeti, nem a transzponált gráfban!) egy erősen összefüggő komponenst. Akkor "kezdünk új" mélységi fát, amikor a rekurzió visszatér és új csúcsot húzunk a veremből
* Műveletigény: a két DFS: 2(n+e), a transzponálás: e

### Példa

* Határozzuk meg eme gráf erősen összefüggő komponenseit

![Feladat](04_Feladat.png)

* Első lépésben tehát bejárjuk mélységien s egy verembe mentjük a csúcsokat befejezéskor

![Első bejárás után](04_ElsoBejaras.png)

* A verem (elforgatva): s = [J, E, H, I, D, C, G, B, F, A
* Most vegyük a transzponáltat és járjuk be úgy, hogy a veremtetővel (A) kezdjünk
    * Nem determinisztikus esetben itt is ábécésorrend szerint választok következő csúcsot
* A következő ábrán a transzponált gráf bejárása látható az első "elakadásig", azaz az első olyan helyzetig, amikor elakad a rekurzív visit()-hívássorozat
* Ezzel tehát a transzponált gráfon előállt a mélységi erdő első fája. A résztvevő csúcsok - A, B, F, G - fogják alkotni az eredeti gráf egyik erősen összefüggő komponensét

![Transzponált bejárása pt. 1](04_Transzponalt1.png)

* Megyünk tovább. A veremtető az F, de ez már fekete, ezért eldobjuk. B és G is így jár. C a következő, azzal folytatjuk

![Transzponált bejárása pt. 2](04_Transzponalt2.png)

* A veremtető a D, ami már fekete, ezért a következővel, az I-vel folytatjuk. Az I-ből kivezető egy darab él egy feketébe vezet, ezért ahogy belépünk az I-be, már lépünk is ki. Ez a rekurzív hívássorozat csak egy elemű, ezért az I önmagában alkot egy komponenst (teljesen világos: mivel az eredeti gráfban I kifoka 0, ezért nem lehet egy másik olyan csúcs sem, amivel **kölcsönösen** elérhető)
* Az előzőt külön nem rajzoltam le
* Végül, a H jönne, ami már fekete, ezért az E-vel folytatjuk, az egy újabb kétcsúcsú komponenst definiál (E, J), és ezzel végeztünk is a bejárással

![Transzponált bejárása pt. 3](04_Transzponalt3.png)

* Az alábbi komponenseket határoztuk meg:

![Végeredmény - erősen összefüggő komponensek](04_Megoldas.png)