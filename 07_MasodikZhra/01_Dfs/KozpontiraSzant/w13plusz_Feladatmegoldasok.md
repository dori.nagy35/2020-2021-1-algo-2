# W13++ - pótóra - 2020.12.11.

## Követelményekről beszéltünk

* Kisszorgalmik felkerültek
* Ponthatárok
* Megajánlott ötös: minimumpont elvárás szóbelin + szóbelik+írásbelik összesen legalább 80% (96p)
    * Ettől gyakjegy még csak 3-as, de lehet jobb
* Decemberi vizsga:  első szóbeli + első zh min. 90% (54p)
    * Azóta ezt is eltöröltem, mert nem végeztem időben a dolgokkal

## Első blokk

### Igaz-e: ha a bejárás során azonos gyökérből két utat is találunk egy adott csúcshoz, és abból az egyik kevesebb élből áll, akkor itt legalább az egyik él előreél?

* Nem, lehet keresztél is. Triviálisan, ha a "gyökér" nem annak a részfának a gyökere, ahol az "adott csúcs" van, de másképp is kijöhet. Persze irányítottnak kell lenni

### Igaz-e: a hurokél visszaél?

* Igaz, ez egy elemes kör ("egyik") éle

### Mi van, ha irányítatlan a gráf és visszaélt találunk? (Megj. A DAG eleve irányított gráfot ír elő)

* Itt lehet "fals pozitív" visszaél is: amikor abból az élből jövünk visszafelé, amiből felfedeztünk egy adott csúcsot azt visszaélnek veheti. Ha feltesszük, hogy nincsenek párhuzamos élek, akkor ezt könnyű ellenőrizni (u és v esetén, ha u szülője már v), különben az is lehet egy stratégia, hogy ha már van címkéje egy élnek, ne adhassunk neki újat. De mindenképp át kell alakítani az algoritmust

## Második blokk

### Osztályozd az előző fejezet utolsó feladatában levő gráf éleit a mélységi bejárás után. Döntsd el, ez a gráf DAG-e

* Íme:

![Lejátszós feladat DFS folytatás](w13plusz_DfsPelda.png)

## Alkalmazások - első blokk

### Milyen gráfra lesz egyértelmű a topologikus sorrend?

* Láncra (mindenkinek pontosan egy elő-utófeltétele van, kivéve a két végét). Megnéztük, minden elágazás (legyen az előfeltételben vagy utófeltételben) növeli a kombinánciók számát

### Milyen gráfra érvényes topologikus sorrend a csúcsok bármely permutációja?

* Üres gráfra. Azaz él nélkülire. Minden más meghatároz legalább két csúcs között egy sorrendet

### Irányítatlan gráfra van-e értelme a topologikus sorrendnek?

* Nincs, mert elő-utófeltétel viszonyokról szól a feladat, ott meg ilyen nincs

## Második blokk

### Igaz-e, hogy mindkét tanult módszer helyes és teljes – azaz minden topologikus sorrend előállítható velük – és csak azok?

* A videóban kb. le van vezetve, bár nem matematikai igényességgel. Az állítás igaz
* A befokos módszerrel nyilván helyes, mert a mindenkori 0 befokúak választhatók. És teljes is, mert épp annyiféle sorrendben tudom ezeket választani (a menet közben kialakuló 0 fokúakra is vonatkoztatva), ahány különböző sorrend van
* A mélységi bejárásosnál is "érezzük" a helyességet. A teljesség nem olyan egyszerű kérdés, hiszen ellentétben a befokos módszerrel, itt nem tudunk egy láncot bármikor megszakítani, hanem ha eg gxökérből elindultunk, azon az ágon végig kell menni. Viszont a "gyökereket" meghatározhatjuk úgy is, hogy azok szemléletesen egy lánc végén vagy közepén vannak, ekkor mégiscsak tudjuk alakítani a bejárás sorrendjét

### Mennyi a topologikus sorrend meghatározásának műveletigénye? Múlik-e a reprezentáción, ill. a tanult algoritmus megválasztásán? Rosszabb-e kördetektálással? (Mondjunk minimális, maximális, átlagos műveletigényt)

* A mélységi bejárásosnál ez egy db mélységi bejárás költsége, ami Ordó(n+e), plusz még egyszer végig kell menni a csúcsokon. Ez nem múlik a reprezentáción sem, illetve annyiban mégis, hogy e értéke sűrű és ritka gráfon eltér a két reprezentáció között. Elsőre azt gondolnánk (és azt hiszem, a videóban is erre jutottunk), hogy emiatt ha tudjuk előre, hogy topologikus rendezést akarunk a gráfon végrehajtani, eleve nem érdemes azt mátrixszal reprezentálni, mert nem lehet sűrű. Ez így jó irány, de gondoljunk arra is, hogy a gráf irányított és előreéles "köröket" akármennyit tartalmazhat, ily módon egy kvázi sűrű gráfra is lehet topologikus sorrendet mondani. A kördetektálás egy elágazás egy olyan élnél, amit mindenképp vizsgálunk, tehát rontani nem ront: sőt, ha nem eleve Ordót mondtam volna, még javítana is, hiszen lehet hogy rögtön az első élnél el is romlik a DAG-tulajdonsás. OK, ha nem lehet hurokél, akkor a másodiknál. OK, ha nem lehet párhuzamos oda-vissza él, akkor a harmadiknál
* A másik esetben egy sorba rakjuk a kezdetben 0 befokú csúcsokat, ez Théta(n). Utána ezen a soron megyünk végig, minden kivétel konstans. Csak az adott csúcsok célcsúcsainál kell frissíeni a kifokokat, amik elérése így konstans, és ezeket kellhet a sorba rakni. Végeredményben a sorba épp n elem kerül, és minden élt meg is nézünk, hogy ezeket eldönthessük, tehát ez is Ordó(n+e). Az Ordóság azért, mert lehet olyan pont, hogy a sor üres még a csúcsok elfogyása előtt - ha nem DAG. És ebből is következik, hogy itt se rontja a kördetektálás a műveletigényt, épp csak javítja ha van kör

### Mennyi az összes lehetséges topologikus sorrend kiírásának műveletigénye?

* Nyilván annyi, amennyi ilyen sorrend van. Az elméleti maximum n!
* Ha ez mind van is, akkor Théta(n!), persze maga a keresés nagyságrendje eltörpül emellett
* A keresésben visszalépéseket kell csinálni, ez nagyságrendileg nem lesz rosszabb, mint maga az eredmény kiírása

### Mit jelent, ha egy gráfban egy darab erősen összefüggő komponens van? És ha n?

* Teljes gráfnál biztosan egy darab van. De ha a gráf csúcsai egy nagy körre húzhatók fel, akkor is igaz lesz ez
* Üres gráfnál biztosan n darab lesz, de pl. ha minden csúcs kifoka vagy befoka 0, akkor is

## KAHOOT
