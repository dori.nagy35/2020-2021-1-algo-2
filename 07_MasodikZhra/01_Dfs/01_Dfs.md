# Gráfok

## Mélységi bejárás

* Angolul Depth-first Search (DFS)
* Elgondolás: Járjuk be egy gráf összes csúcsát (nem összefüggő esetben is az összeset!) úgy, hogy egy tetszőlegesen kiválasztott csúcsból elindulva, abból egy tetszőleges úton a "falig" megyünk, aztán visszafordulunk (egy csúcsnyit), megint elmegyünk olyan mélyre, ahogy csak tudunk és ezt ismételjük egész addig, amíg nem marad hova visszafordulni
    * Igazából ez a preorder bejárás általánosításaként fogható fel
* Ekkor összefüggő esetben épp végeztünk, nem összefüggő esetben keresünk egy újabb csúcsot
    * Az a feature, amit a szélességi bejárásnál felvetettünk, itt "gyárilag" megvan
    * Ugyanakkor mivel itt az egész gráfot járjuk be, nincs is paraméterként átadott önkényesen meghatározott startcsúcs
* A "falig" gyakorlatilag azt jelenti, hogy már nincs eddig érintetlen szomszédja az adott csúcsnak

```
dfs(g : Graph)
  FOR EACH u eleme g.V
    u.c := W
    u.pi := NULL
    u.d := 0
    u.f := 0
  time := 0
  FOR EACH u eleme g.V
    HA u.c = W
      visit(u, time)
```

```
visit(u : Vertex, time& : N)
  u.c := G
  time := time+1
  u.d := time
  process(u)
  FOR EACH v eleme neighbours(u)
    HA v.c = W
      v.pi := u
      visit(v, time)
  time := time+1
  u.f := time
  u.c := B
```

* A bejárás nem meglepő módon rekurzív függvénnyel ragadható meg leginkább
* Az alapalgoritmus előbb inicializál: beállítja minden csúcs
    * színét fehérre (felfedezetlen)
    * szülőjét nullra (még nem jutottunk el ide)
    * felfedezési (discovery) vagy mélységi (depth) idejét 0-ra (még nem fedeztük fel)
    * befejezési (finalize) idejét 0-ra (még nem hagytuk el)
* Utána elindít egy globális időt 0-ról
* Majd minden csúcsra meghívja a rekurzív függvényt, már ami fehér (kezdetben persze mind az)
* A visit függvény beszürkíti az adott csúcsot, ez jelenti azt, hogy épp őt vizsgáljuk, azaz felfedeztük
    * Ennek megfelelően a felfedezési számát is megkapja, ami az első időpillanat lesz, azaz 1 -- előzőleg a 0-t növeltük
    * Bejárjuk a gyerekeit, ha olyan volt, akit most értünk el először, akkor beállítjuk a szülőjét u-ra, és meghívjuk vele a visitet
    * Ekkor újra lejátszódik a szürkítés és a felfedezési szám hozzárendelése (az idő változót referencia szerint adjuk át, tehát az "hat" a hívó szinten is!)
    * Amikor az összes gyerekkel és a rekurzió miatt annak minden további leszármazottjával végzünk, újra növeljük az időt és beállítjuk a befejezési időt és végül feketére állítjuk a csúcsot
* Néhány következmény:
    * Az idő egyedi. 1-től 2n-ig vannak a d-k és f-ek, mindegyik szám pont egyszer
    * Senkinek sem lesz 0 semmilyen száma a végére (time 0-ról indul, de az első hozzárendelés előtt már növelem)
    * Minden esetben a d kisebb, mint az f (egy csúcsba előbb megyünk bele és utána lépünk ki)
    * A fehér szín a még nem látogatott csúcsot jelenti (azaz d és f is 0)
    * A szürke szín az épp látogatás alatt levő csúcsot jelenti (azaz d nem 0, de f igen)
    * A fekete szín a már elhagyott csúcsot jelenti (se d, se f nem 0)
    * Egyszerre több szürke csúcs is lehet, mert ha egy csúcs szürke, az ő összes szülője is az, hiszen addig nem léphettünk ki egy csúcsból, amíg a rekurzió nem végzett a gyerekeivel
    * Egy csúcs befejezési száma pont annyival nagyobb a mélységi számnál, mint (ahány leszármazottat onnan elsőként elértünk * 2) + 1
        * Hiszen ha elérünk pl. 2 gyereket, és az egyik gyereknek még van egy érintetlen szomszédja, akkor összességében mindhárom kap egy mélységi és befejezési számot, ami 3*2, és eleve van még egy különbség minden csúcs mélységi és befejezési száma között
    * Lehet több NULL π is, ha a gráf nem összefüggő
    * Figyelem: ez a π-k által meghatározott mélységi fa (vagy mélységi erdő) egy adott csúcsra vetített leszármazott-számát adja meg, de semmiképpen se az eredeti gráfban vett hasonló értéket (azaz egy részfa méretét), hiszen kör esetén ez pl. eleve végtelen lenne (mivel önmagát is többször elérné), de a mélységi bejárás értelemszerűen minden csúcsot egyszer ér el!
    * Az algoritmus működik súlyozástól és irányítottságtól függetlenül
        * Irányítatlan esetben kétszer megy végig az éleken, de csak az első esetben "számolja azokat"
* A műveletigényről:
    * Az inicializálás a csúcsok számától függ
    * A visitet biztosan a "csúcsok számaszor" hívjuk meg: a külső szinten egyszer, aztán ez valahány gyereket beszínez -- azokra meg is hívja --, majd a "maradék" el nem ért gyerekkel ezt újrakezdjük. Mivel csak a fehér gyerekekre hívódik meg a rekurzió, de a külső ciklus miatt az összes fehér gyerekre garantáltan meg is hívódik ezért ez egyértelmű
    * Viszont a visitben a ciklus csak végigmegy az összes szomszédon, ahhoz, hogy megállapíthassa, rajta vajon meg kell-e újra hívni a rekurzív függvényt
    * Összességében tehát a ciklus n+e-szer fut le, ez a domináns tag, ez tehát a műveletigény
