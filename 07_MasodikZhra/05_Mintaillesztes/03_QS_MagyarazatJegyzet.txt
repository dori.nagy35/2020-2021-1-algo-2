QS
--

Hasonló

initShift()
Implementációban a shift is tömb
És ez is P-től függ

Végigmegy az ábécén
d = |Szigma|
Ez a T ábécéje

shift : Szigma -> [1..m+1]

Végigmegyünk a mintán
Ha találunk adott betűt, akkor a hátulról vett indexével frissítjük a hozzá tartozó értéket

Lásd példa 1. - példa 2.

Sorok: 1 elem, csökkenő

Végére: shift[s] =
 = m+1, ha s nem szerepel P-ben
 = k, ha a betű utolsó előfordulása hátulról k indexen van

Fő algoritmus:

s: eltolás
s+m: utolsó indexe P-nek T-ben, ez max n lehet

T[s+1..s+m] = P[1..m] lásd BF-nál

HA s+m = n
"break" - ami szabálytalan, de értjük. Fontos, mert különben végtelen ciklus (implementációs kérdés)
Másik ágon kell T[s+m+1]

HA s+m < n

Tehát ha nem az utolsó elméletben lehetséges illeszkedést néztük, akkor a következő karakter shiftje alapján megyünk
(akár illeszkedtünk, akár nem)

következő karakter: minta utáni, bárhol is romlott el

Ez túlugorhat, de akkor a következő ciklusfeltétel-ellenőrzés hamis lesz

Amikor a shift(s) = m+1, akkor az azt jelenti, azt a betűt átugorjuk, mivel nincs P-ben

Példa 1. első kör: s=0 --> s=5
Második kör: s=5 --> s=8 (és 9-től ellenőrzök, stb.)

Műveletigény innen világos:
minimális ha első betűnél elromlik és mindig m+1-eket ugrunk; maximális ha a végén és kicsiket
