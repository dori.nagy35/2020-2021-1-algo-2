# Gráfok - Legrövidebb utak minden csúcspárra

* Erre se tértünk ki órán
* Vadász Péter anyaga ebben a mappában a shortest_path_for_all_vertices.pdf fájl
* Kommentjeim: https://youtu.be/a4fqsn_Pnh4
    * Illetve ebben a mappában a 00_MagyarazatJegyzet.txt fájl tartalma

## Floyd--Warshall-algoritmus

* Lásd PDF és videó

## Warshall-algoritmus (tranzitív lezárt)

* Lásd PDF és videó
