# Gráfok

## Prim-algoritmus

* Futtassuk le a Prim-algoritmust az alábbi gráfon, minden körben jeöljük d-t, π-t és minQ-t

![Prim példa](03_PrimPelda.png)

|      | minQ                                  | (d,π) | A        | B        | C        | D        | E        | F        |
| ---- | ------------------------------------- | ----- | -------- | -------- | -------- | -------- | -------- | -------- |
| INIT | <(A,0),(B,∞),(C,∞),(D,∞),(E,∞),(F,∞)> |       | (0,NULL) | (∞,NULL) | (∞,NULL) | (∞,NULL) | (∞,NULL) | (∞,NULL) |
| A    | <(B,1),(D,3),(C,4),(E,∞),(F,∞)>       |       | done     | (1,A)    | (4,A)    | (3,A)    |          |          |
| B    | <(C,2),(D,3),(E,4),(F,∞)>             |       |          | done     | (2,B)    |          | (4,B)    |          |
| C    | <(E,1),(D,3),(F,∞)>                   |       |          |          | done     |          | (1,C)    |          |
| E    | <(D,1),(F,5)>                         |       |          |          |          | (1,E)    | done     | (5,E)    |
| D    | <(F,5)>                               |       |          |          |          | done     |          |          |
| F    | <>                                    |       |          |          |          |          |          | done     |

* Az eredmény:

![Prim megoldás](03_PrimMegoldas.png)
